package ru.chernitsov;


import static java.lang.Math.PI;

public class Matrix {
    private int width;
    private int height;
    private float[][] mat;

    public Matrix (int height ,int width){
        this.height = height;
        this.width = width;
        mat = new float[height][width];
        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                mat[i][j] = 0.f;
            }
        }
    }

    public boolean equal(Matrix one, Matrix two){
        if (one.height != two.height || one.width != two.width){
            return false;
        }
        for (int i = 0; i < one.height; ++i){
            for (int j = 0; j < one.width; ++j) {
                if (one.mat[i][j] != two.mat[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public float[][] getMat() {
        return mat;
    }

    public void setElem(int i, int j, float elem){
        this.mat[i][j] = elem;
    }

    public float getElem(int i, int j){
        return this.mat[i][j];
    }

    public void setMat(float[][] mat) {
        if (this.width != mat[1].length) {
            throw new RuntimeException("Matrix have different dimension");
        }
        for (int i = 0; i < this.height; ++i) {
            for (int j = 0; j < this.width; ++j) {
                this.mat[i][j] = mat[i][j];
            }
        }
    }

    public static Matrix multiply(Matrix one, Matrix two){ //one * two
        if (one.width != two.height) {
            throw new RuntimeException("matrix shold be n*m and m*k");
        }
        Matrix Answ = new Matrix(one.height, two.width);
        for (int i = 0; i < one.height; ++i) {
            for (int j = 0; j < two.width; ++j) {
                float sum = 0;
                float[] str = one.mat[i];
                for (int k = 0; k < two.width; ++k) {
                    sum += str[k] * two.mat[k][j];
                }
                Answ.mat[i][j] = sum;
            }
        }
        return Answ;
    }

    public static Matrix sum(Matrix one, Matrix two){
        if (one.width != two.width || one.height != two.height) {
            throw new RuntimeException("Matrix have different dimension");
        }
        Matrix Answ = new Matrix(one.height, two.width);
        for (int i = 0; i < one.height; ++i) {
            for (int j = 0; j < two.width; ++j) {
                Answ.mat[i][j] = one.mat[i][j] + two.mat[i][j];
            }
        }
        return Answ;
    }

    public static Matrix multOnSk(Matrix one, float constant){
        Matrix Answ = new Matrix(one.height, one.width);
        for (int i = 0; i < one.height; ++i) {
            for (int j = 0; j < one.width; ++j) {
                Answ.mat[i][j] = one.mat[i][j] * constant;
            }
        }
        return Answ;
    }

    public static Matrix differense(Matrix one, Matrix two) {
        return sum(one, multOnSk(two, -1.f));
    }

    public static Matrix division(Matrix one, float constant) {
        return multOnSk(one, 1.f / constant);
    }

    // Trnnsponate
    public static Matrix transpose(Matrix one) {
        Matrix Answ = new Matrix(one.width, one.height);
        for (int i = 0; i < one.height; ++i) {
            for (int j = 0; j < one.width; ++j) {
                Answ.mat[j][i] = one.mat[i][j];
            }
        }
        return Answ;
    }



    // getOposit
    public void selfInversion(Matrix mat) {
        if (mat.width != mat.height) {
            throw new RuntimeException("Matrix is not square");
        }
        float temp;
        int N = mat.height;
        float [][] E = new float [N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                E[i][j] = 0f;

                if (i == j)
                    E[i][j] = 1f;
            }

        for (int k = 0; k < N; k++) {
            temp = mat.mat[k][k];

            for (int j = 0; j < N; j++) {
                mat.mat[k][j] /= temp;
                E[k][j] /= temp;
            }

            for (int i = k + 1; i < N; i++) {
                temp = mat.mat[i][k];

                for (int j = 0; j < N; j++)
                {
                    mat.mat[i][j] -= mat.mat[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int k = N - 1; k > 0; k--) {
            for (int i = k - 1; i >= 0; i--) {
                temp = mat.mat[i][k];

                for (int j = 0; j < N; j++) {
                    mat.mat[i][j] -= mat.mat[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                mat.mat[i][j] = E[i][j];

    }

    public static Matrix inversion(Matrix mat) {
        if (mat.width != mat.height) {
            throw new RuntimeException("Matrix is not square");
        }
        Matrix Answ = new Matrix(mat.height, mat.width);
        float temp;
        int N = mat.height;
        float [][] E = new float [N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                E[i][j] = 0f;

                if (i == j)
                    E[i][j] = 1f;
            }

        for (int k = 0; k < N; k++) {
            temp = mat.mat[k][k];

            for (int j = 0; j < N; j++) {
                mat.mat[k][j] /= temp;
                E[k][j] /= temp;
            }

            for (int i = k + 1; i < N; i++) {
                temp = mat.mat[i][k];

                for (int j = 0; j < N; j++)
                {
                    mat.mat[i][j] -= mat.mat[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int k = N - 1; k > 0; k--) {
            for (int i = k - 1; i >= 0; i--) {
                temp = mat.mat[i][k];

                for (int j = 0; j < N; j++) {
                    mat.mat[i][j] -= mat.mat[k][j] * temp;
                    E[i][j] -= E[k][j] * temp;
                }
            }
        }

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                Answ.mat[i][j] = E[i][j];
        return Answ;
    }
    //
    public void addElems1D(int i, float[] elems){
        if (this.width > 1){
            throw new RuntimeException("Matrix must be n x 1");
        }
        for (int it = 0; it < elems.length; ++it){
            this.mat[i + it][0] += elems[it];
        }
    }
    public void addElems1D(int i, float[] elems, float h){
        if (this.width > 1){
            throw new RuntimeException("Matrix must be n x 1");
        }
        for (int it = 0; it < elems.length; ++it){
            this.mat[i + it][0] += h * elems[it];
        }
    }
    //разрез
    public static float[] cutOutOf(int start, int len, Matrix matrix ){
        if (matrix.width > 1){
            throw new RuntimeException("Matrix must be n x 1");
        }
        float[] array = new float[len];
        for (int i = 0 ; i < len; ++i){
            array[i] = matrix.mat[start + i][0];
        }
       return array;
    }

    protected void Normalize(){
        for (int i = 6; i < 9; ++i){
            float elem = this.getElem(i, 0) % (float) PI * 2;
            this.setElem(i,0,elem);
        }
    }

}
