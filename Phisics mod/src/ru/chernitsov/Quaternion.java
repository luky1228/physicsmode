package ru.chernitsov;

public class Quaternion extends Matrix{

    public Quaternion (float r, float i, float j, float k){
        super(4,1);
        this.setElem(0,0, r);
        this.setElem(1,0, i);
        this.setElem(2,0, j);
        this.setElem(3,0, k);
    }

    public float getR() {
        return getElem(0,0);
    }

    public void setR(float r) {
        this.setElem(0,0, r);
    }

    public float getI() {
        return getElem(1,0);
    }

    public void setI(float i) {
        this.setElem(1,0, i);
    }

    public float getJ() {
        return getElem(2,0);
    }

    public void setJ(float j) {
        this.setElem(2,0, j);
    }

    public float getK() {
        return getElem(3,0);
    }

    public void setK(float k) {
        this.setElem(3,0, k);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static Quaternion multOnSk(Quaternion one, float sk) {
        return new Quaternion(one.getR() * sk,one.getI() * sk,one.getJ() * sk,one.getK() * sk);
    }

    // умножение кватов
    public static Quaternion multiplication(Quaternion first, Quaternion second) { // first * second
        Quaternion answ = new Quaternion(0,0,0,0);
        float box = 0.f;
        float fr = first.getR();
        float fi = first.getI();
        float fj = first.getJ();
        float fk = first.getK();
        float sr = second.getR();
        float si = second.getI();
        float sj = second.getJ();
        float sk = second.getK();
        box = fr * sr - fi * si - fj * sj - fk * sk;
        answ.setR(box);
        box = fr * si + fi * sr + fj * sk - fk * sj ;
        answ.setI(box);
        box = fr * sj + fj * sr + fk * si - fi * sk ;
        answ.setJ(box);
        box = fr * sk + fk * sr + fi * sj - fj * si;
        answ.setK(box);
        return answ;
    }
    public static Quaternion vec2quat(float[] vec){
        return new Quaternion(0,vec[0],vec[1],vec[2]);
    }
    public static float[] quat2vec(Quaternion q){
        float[] answ = {q.getI(), q.getJ(), q.getK()};
        return answ;
    }

    //обратный
    public static Quaternion inverse(Quaternion quaternion){
        float len = norm(quaternion);
        if (len == 0){
            return new Quaternion(0, 0, 0, 0);
        }
        Quaternion it = conjugate(quaternion);
        return new Quaternion(it.getR()/len,it.getI()/len,it.getJ()/len,it.getK()/len);
    }
    public static float norm (Quaternion it){
        return sqr(it.getR()) + sqr(it.getI()) + sqr(it.getJ()) + sqr(it.getK());
    }

    private static float sqr(float r) {
        return r * r;
    }
    public static Quaternion conjugate(Quaternion quat){
        return  new Quaternion(quat.getR(),-1 * quat.getI(),-1 * quat.getJ(),-1 * quat.getK());
    }


    // поворот вектора
    public static Quaternion vecTurn(float[] vec, Quaternion quat) {
        return multiplication(quat, multiplication(vec2quat(vec), inverse(quat)));
    }
    public static Quaternion backVecTurn(float[] vec, Quaternion quat) {
        return  multiplication(multiplication(inverse(quat), vec2quat(vec)), quat);
    }
    // дифиринцирование
    public static Quaternion globalDerivation (float[] angVel, Quaternion one) {
        Quaternion Vel = vec2quat(angVel);
        return multiplication(multOnSk(Vel, 0.5f), one);
    }

    public static Quaternion localDerivation (float[] angVel, Quaternion one) {
        Quaternion Vel = vec2quat(angVel);
        return empty_dot(one, Vel);
    }
    public static Quaternion Vec4d2Qternion(float[] vec) {
        return new Quaternion(vec[0], vec[1], vec[2], vec[3]);
    }
    public static float[] Qternion2Vek4(Quaternion q) {
        float[] t = {q.getR(), q.getI(), q.getJ(), q.getK()};
        return t;
    }
    public static void Print(Quaternion q){
        LoggerHolder.logger.debug(String.format("%f %f %f %f", q.getR(), q.getI(), q.getJ(), q.getK()));
    }
    public static Quaternion empty_dot(Quaternion q, Quaternion omega ){
        return new Quaternion(0.5f*(-q.getI() * omega.getI() - q.getJ() * omega.getJ() - q.getK() * omega.getK()),
                0.5f*(q.getR() * omega.getI() - q.getK() * omega.getJ() + q.getJ() * omega.getK()),
                0.5f*(q.getK() * omega.getI() + q.getR() * omega.getJ() - q.getI() * omega.getK()),
                0.5f*(-q.getJ() * omega.getI() + q.getI() * omega.getJ() + q.getR() * omega.getK()));
    }

    public static float[]  toEulerAngle( Quaternion q) {
        float roll, pitch, yaw;
        float w = q.getR(), x = q.getI(), y = q.getJ(), z = q.getK();
        float sinr = 2.f * (w * x + y * z);
        float cosr = 1.f - 2.f * (x * x + y * y);
        roll = (float) Math.atan2(sinr, cosr);
        float sinp = 2.f * (w * y - z * x);
        if (Math.abs(sinp) >= 1)
            pitch =  (float) Math.copySign( Math.PI / 2, sinp);
        else
            pitch = (float) Math.asin(sinp);
        float siny = 2.f * (w * z + x * y);
        float cosy = 1.f - 2.f * (y * y + z * z);
        yaw = (float) Math.atan2(siny, cosy);
        return new float[]{roll, pitch, yaw};
    }

    public static Quaternion toNorm(Quaternion it){
        float len = Quaternion.norm(it);
        return new Quaternion(it.getR()/len,it.getI()/len,it.getJ()/len,it.getK()/len);
    }

    public static float round(float tmp){
        return (float) (int) (Math.abs(tmp) >= 0.04f ? tmp  * 10000 : 0) / 10000;
    }
}
