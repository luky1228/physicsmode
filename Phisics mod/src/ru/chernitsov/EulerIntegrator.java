package ru.chernitsov;


public class EulerIntegrator extends Integrator {
    Copter state;
    public EulerIntegrator (Copter initial){
        super(initial);
        state = initial;
    }
    public Copter step(float h, float f1, float f2, float f3, float f4) {
        calcU1(f1, f2, f3, f4);
        calcU2_4(f1, f2, f3, f4, state);
        Matrix der =new Matrix(13, 1);
        der.addElems1D(0, position(state.state, Matrix.cutOutOf(3, 3, state.state)), h);
        der.addElems1D(3, linearVel(state.state, state.m), h);
        der.addElems1D(6, angularVel(Matrix.cutOutOf(6, 3, state.state), state), h);
        der.addElems1D(9, quaternion(Matrix.cutOutOf(6, 3, state.state), state.state), h);
        state.state = Matrix.sum(state.state, der);
        state.state.Normalize();
        Copter.Print_state(state);
        return state;
    }
}
