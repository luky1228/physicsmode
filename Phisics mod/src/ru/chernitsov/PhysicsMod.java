package ru.chernitsov;

import com.yuriy.client.BplaException;
import com.yuriy.client.Context;
import com.yuriy.client.Module;
import com.yuriy.json.JsonException;
import com.yuriy.json.JsonValue;
import com.yuriy.logging.Logger;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class PhysicsMod implements Module {
    private float M1, M2, M3, M4;
    private Quaternion q;
    private float dt;
    private String IType, UseDefaltModel;
    private float max, min;
    private float cp, ci, cd;
    private float w, l, tx, ty, tz;
    private Logger logger;

    @Override
    public void init(JsonValue params, Context ctx) throws JsonException {
        logger = ctx.getLogger();
        M1 = params.get("f1").asFloat();
        M2 = params.get("f2").asFloat();
        M3 = params.get("f3").asFloat();
        M4 = params.get("f4").asFloat();
        cp = params.get("cp").asFloat();
        ci = params.get("ci").asFloat();
        cd = params.get("cd").asFloat();
        q = new Quaternion(params.get("q0").asFloat(), params.get("q1").asFloat(),
                params.get("q2").asFloat(), params.get("q3").asFloat());
        dt = params.get("dt").asFloat();
        IType = params.get("integrator").asString();
        if (params.get("UseDefaltModel").asBool()) {
            w = params.get("w").asFloat();
            l = params.get("l").asFloat();
            tx = params.get("tx").asFloat();
            ty = params.get("ty").asFloat();
            tz = params.get("tz").asFloat();
        }
        try {
            ctx.allocate("quadcopter_states", 3 + 3 + 3 + 1 + 3 + 4, AtomicInteger::new);
        } catch (BplaException e) {
            logger.error(e.getMessage());
        }
        logger.debug(IType);
        LoggerHolder.logger = logger;
    }

    private void post_state(AtomicInteger S_x,
                            AtomicInteger S_y,
                            AtomicInteger S_z,
                            AtomicInteger V_x,
                            AtomicInteger V_y,
                            AtomicInteger V_z,
                            AtomicInteger W_x,
                            AtomicInteger W_y,
                            AtomicInteger W_z,
                            AtomicInteger r,
                            AtomicInteger i,
                            AtomicInteger j,
                            AtomicInteger k,
                            AtomicInteger F1,
                            AtomicInteger F2,
                            AtomicInteger F3,
                            AtomicInteger F4,
                            float f1,
                            float f2,
                            float f3,
                            float f4,
                            Copter copter) {
        S_x.set(Float.floatToIntBits(-1 * copter.state.getElem(0, 0)));
        S_y.set(Float.floatToIntBits(-1 * copter.state.getElem(1, 0)));
        S_z.set(Float.floatToIntBits(-1 * copter.state.getElem(2, 0)));
        V_x.set(Float.floatToIntBits(copter.state.getElem(3, 0)));
        V_y.set(Float.floatToIntBits(copter.state.getElem(4, 0)));
        V_z.set(Float.floatToIntBits(copter.state.getElem(5, 0)));
        W_x.set(Float.floatToIntBits(copter.state.getElem(6, 0)));
        W_y.set(Float.floatToIntBits(copter.state.getElem(7, 0)));
        W_z.set(Float.floatToIntBits(copter.state.getElem(8, 0)));
        r.set(Float.floatToIntBits(copter.state.getElem(9, 0)));
        i.set(Float.floatToIntBits(copter.state.getElem(10, 0)));
        j.set(Float.floatToIntBits(copter.state.getElem(11, 0)));
        k.set(Float.floatToIntBits(copter.state.getElem(12, 0)));
        F1.set(Float.floatToIntBits(-1 * f1));
        F2.set(Float.floatToIntBits(1 * f2));
        F3.set(Float.floatToIntBits(-1 * f3));
        F4.set(Float.floatToIntBits(1 * f4));

    }

    @Override
    public void run(Context ctx) {
        AtomicInteger S_x;
        AtomicInteger S_y;
        AtomicInteger S_z;
        AtomicInteger V_x;
        AtomicInteger V_y;
        AtomicInteger V_z;
        AtomicInteger W_x;
        AtomicInteger W_y;
        AtomicInteger W_z;
        AtomicInteger r;
        AtomicInteger i;
        AtomicInteger j;
        AtomicInteger k;
        AtomicInteger F1;
        AtomicInteger F2;
        AtomicInteger F3;
        AtomicInteger F4;
        try {
            ArrayList<AtomicInteger> atomic_variables = ctx.get("quadcopter_states");
            S_x = atomic_variables.get(0);
            S_y = atomic_variables.get(1);
            S_z = atomic_variables.get(2);
            V_x = atomic_variables.get(3);
            V_y = atomic_variables.get(4);
            V_z = atomic_variables.get(5);
            W_x = atomic_variables.get(6);
            W_y = atomic_variables.get(7);
            W_z = atomic_variables.get(8);
            r = atomic_variables.get(9);
            i = atomic_variables.get(10);
            j = atomic_variables.get(11);
            k = atomic_variables.get(12);
            F1 = atomic_variables.get(13);
            F2 = atomic_variables.get(14);
            F3 = atomic_variables.get(15);
            F4 = atomic_variables.get(16);
            logger.debug("Copter allocated");
            Thread.yield();
        } catch (Exception e) {
            throw new RuntimeException("unable to get memory");
        }
        Timer time = new Timer();
        time.update();
        Copter copter2 = new Copter();

        float init = 0.8085f;
        float f1 = init, f2 = init, f3 = init, f4 = init;

        Integrator integrator;
        if (IType.equals("RK")) {
            logger.debug("RK used");
            integrator = new IntegratorRK(copter2);
        } else {
            if (IType == "BackEuler") {
                logger.debug("BE used");
                integrator = new BackEulerIntegrator(copter2);
            } else {
                integrator = new EulerIntegrator(copter2);
            }
        }

        copter2 = integrator.step((float) time.getDelta(), f1, f2, f3, f4);
        Copter.Print_state(copter2);
        post_state(S_x, S_y, S_z, V_x, V_y, V_z, W_x, W_y, W_z, r, i, j, k, F1, F2, F3, F4, f1, f2, f3, f4, copter2);
        int cntr = 0;

        while (cntr < 300) {
            cntr += 1;
            try {
                copter2 = integrator.step((float) time.getDelta(), f1, f2, f3, f4);
            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }

            try {
                post_state(S_x, S_y, S_z, V_x, V_y, V_z, W_x, W_y, W_z, r, i, j, k, F1, F2, F3, F4, f1, f2, f3, f4, copter2);
                time.update();
                Thread.sleep(10);

            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }
        }

        f1 = M1;
        f2 = M2;
        f3 = M3;
        f4 = M4;


        copter2.SetQ(q);
        while (cntr < 500) {
            cntr += 1;
            try {
                copter2 = integrator.step((float) time.getDelta(), f1, f2, f3, f4);
            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }

            try {
                post_state(S_x, S_y, S_z, V_x, V_y, V_z, W_x, W_y, W_z, r, i, j, k, F1, F2, F3, F4, f1, f2, f3, f4, copter2);
                time.update();
                Thread.sleep(10);

            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }
        }
        copter2 = integrator.step((float) time.getDelta(), f1, f2, f3, f4);
        Copter.Print_state(copter2);
        float t;
        float extraT = 0;
        PID pid_x = new PID(cp, ci, cd);
        PID pid_y = new PID(cp, ci, cd);
        PID pid_z = new PID(cp, ci, cd);
        float x;
        float y;
        float z;
        float res;
        while (true) {
            try {

                t = (float) time.getDelta();
                if (t < dt) {
                    extraT += t;
                }
                if (extraT >= dt) {
                    copter2.SetQ(Quaternion.toNorm(Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, copter2.state))));
                    float[] eulerAng = Quaternion.toEulerAngle(Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, copter2.state)));
                    x = eulerAng[0] / (float) Math.PI * 180;
                    y = eulerAng[1] / (float) Math.PI * 180;
                    z = eulerAng[2] / (float) Math.PI * 180;
                    logger.debug("x " + x + " y " + y + " z " + z);
                    res = pid_x.calcPID(x, copter2, dt);
                    f3 += res / 2;
                    f4 += res / 2;
                    

                    res = pid_y.calcPID(y, copter2, dt);
                    f1 += res / 2;
                    f4 += res / 2;
                    

                    res = pid_z.calcPID(z, copter2, dt);
                    f2 += res / 2;
                    f4 += res / 2;
                

                    copter2 = integrator.step(dt, f1, f2, f3, f4);
                    f1 = Integrator.round(f1, 5);
                    f2 = Integrator.round(f2, 5);
                    f3 = Integrator.round(f3, 5);
                    f4 = Integrator.round(f4, 5);
                    extraT -= dt;
                }
                while (t >= dt) {
                    copter2.SetQ(Quaternion.toNorm(Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, copter2.state))));
                    float[] eulerAng = Quaternion.toEulerAngle(Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, copter2.state)));
                    x = eulerAng[0];
                    y = eulerAng[1];
                    z = eulerAng[2];
                    res = pid_x.calcPID(x, copter2, dt);
                    f3 += res / 2;
                    f4 += res / 2;
                    

                    res = pid_y.calcPID(y, copter2, dt);
                    f1 += res / 2;
                    f4 += res / 2;
                    
                    res = pid_z.calcPID(z, copter2, dt);
                    f2 += res / 2;
                    f4 += res / 2;


                    copter2 = integrator.step(dt, f1, f2, f3, f4);
                    f1 = Math.min(max, f1);
                    f2 = Math.min(max, f2);
                    f3 = Math.min(max, f3);
                    f4 = Math.min(max, f4);

                    f1 = Math.max(min, f1);
                    f2 = Math.max(min, f2);
                    f3 = Math.max(min, f3);
                    f4 = Math.max(min, f4);
                    t -= dt;
                    if (t < dt) {
                        extraT += t;
                    }
                }
            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }

            try {
                copter2.SetQ(Quaternion.toNorm(Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, copter2.state))));
                post_state(S_x, S_y, S_z, V_x, V_y, V_z, W_x, W_y, W_z, r, i, j, k, F1, F2, F3, F4, f1, f2, f3, f4, copter2);
                time.update();
                Thread.yield();

            } catch (Exception e) {
                throw new RuntimeException("unable to integrate");
            }
        }
    }
}
