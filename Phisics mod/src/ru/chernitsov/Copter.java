package ru.chernitsov;


public class Copter {
    float m = 0.33f;
    float l = 0.03973f;
    Matrix state = new Matrix(13, 1);
    float[] tenzorOfInertion = {0.00001395f, 0.00001436f, 0.00002173f};
    
    public Copter(){
        float[] eta = {0, 0, -20};
        float[] v  = {0, 0, 0};
        float[] omega  = {0,0, 0};
        float[] lambda  = {1, 0, 0, 0};
        this.state.addElems1D(0, eta);
        this.state.addElems1D(3, v);
        this.state.addElems1D(6,omega);
        this.state.addElems1D(9, lambda);
        this.state.setElem(12, 0, lambda[3]);
    }
    public void SetQ(Quaternion q){
        float[] lambda  = Quaternion.Qternion2Vek4(q);
        this.state.setElem(9, 0, lambda[0]);
        this.state.setElem(10, 0, lambda[1]);
        this.state.setElem(11, 0, lambda[2]);
        this.state.setElem(12, 0, lambda[3]);
    }

    public static void Print_state(Copter copter1){
        Matrix state = copter1.state;
        LoggerHolder.logger.debug(String.format("Position: %f %f %f", state.getElem(0, 0), copter1.state.getElem(1, 0), copter1.state.getElem(2, 0)));
        LoggerHolder.logger.debug(String.format("Quarernion: %f %f %f %f", state.getElem(9, 0), state.getElem(10, 0), state.getElem(11, 0), state.getElem(12, 0)));
    }
}
