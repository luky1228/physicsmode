package ru.chernitsov;

public abstract class  Integrator {
    Copter copter;
    private float U1;
    private float U2;
    private float U3;
    private float U4;
    private float[] g = {0.f, 0.f, 9.8f};

    Integrator (Copter init){
        this.copter = init;
    }
    Copter step(float h, float f1, float f2, float f3, float f4){
        return copter;
    }

    protected static float round(float number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        float tmp = number * pow;
        return (float) (int) ((tmp - (int) tmp) >= 0.5f ? tmp + 1 : tmp) / pow;
    }


    public void calcU1(float f1, float f2, float f3, float f4){
        this.U1 = -1 * (f1 + f2 + f3 + f4);
    }
    public void calcU2_4(float f1, float f2, float f3, float f4, Copter c){
        this.U2 = (float) Math.sqrt(2)/2 * c.l * (f3 - f2 - f1 + f4);
        this.U3 = (float)Math.sqrt(2)/2 * c.l * (f1 - f2 - f3 + f4);
        this.U4 = (f2 - f1 - f3 + f4);
    }
    public float[] linearVel(Matrix state, float mass) {
        float[] vel;
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, state));
        vel = Quaternion.quat2vec(Quaternion.vecTurn(g, q));
        vel[2] += 1/mass * U1;
        for (int i = 0; i < 3; ++i){
            vel[i] = Integrator.round(vel[i], 4);
        }
        return vel;
    }
    public float[] angularVel(float[] ang, Copter c){
        float[] dAng = {0, 0, 0};
        dAng[0] = U2 / c.tenzorOfInertion[0] - ((c.tenzorOfInertion[2] - c.tenzorOfInertion[1])/ c.tenzorOfInertion[0] * ang[1] * ang[2] );
        dAng[1] = U3 / c.tenzorOfInertion[1] - ((c.tenzorOfInertion[0] - c.tenzorOfInertion[2])/ c.tenzorOfInertion[1] * ang[0] * ang[2] );
        dAng[2] = U4 / c.tenzorOfInertion[2] - ((c.tenzorOfInertion[1] - c.tenzorOfInertion[0])/ c.tenzorOfInertion[0] * ang[1] * ang[0] );
        return dAng;
    }


    public float[] position(Matrix state, float[] vel) {
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, state));
        return Quaternion.quat2vec(Quaternion.backVecTurn(vel, q));

    }
    public float[] quaternion(float[] angulsrVel, Matrix state){
        Quaternion q = Quaternion.Vec4d2Qternion(Matrix.cutOutOf(9, 4, state));
        return Quaternion.Qternion2Vek4(Quaternion.localDerivation(angulsrVel, q));
    }

}
