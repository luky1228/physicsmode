package ru.chernitsov;

public class Main{
    public static void main(String[] args) {
        Quaternion q1 = new Quaternion(1.0077205f, -0.02077007f, 0.0f, 0.0f);
        Quaternion.Print(Quaternion.toNorm(q1));
        LoggerHolder.logger.debug(Double.toString(Quaternion.toEulerAngle(Quaternion.toNorm(q1))[0]/ Math.PI * 180));
    }
}
