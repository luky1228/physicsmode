package ru.chernitsov;

public class PID {
    float cp;
    float ci;
    float cd;
    float prevError;
    float integral;

    public PID(float cp, float ci,float cd){
        this.cp = cp;
        this.ci = ci;
        this.cd = cd;
        this.prevError = 0;
        this.integral = 0;
    }

    public float calcPID(float er, Copter c, float dt){
        float p = - er;
        this.integral += (- er) * dt;
        float d = 0;
        if (this.prevError != 0){
            d = (- er - this.prevError) / dt;
        }
        this.prevError = - er;
        return cp * p + ci *  this.integral + cd * d;

    }
}
